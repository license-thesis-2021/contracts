// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./EntityOfferRegistry.sol";

contract EntityRegistry {
    mapping(address => EntityOfferRegistry) public entityRegistries;

    event EntityAdded(address indexed entityOwner, EntityOfferRegistry indexed newEntityOfferRegistry);

    modifier onlyNewSender() {
        require(address(entityRegistries[msg.sender]) == address(0),
            "Registry already exists for this entity address");
        _;
    }

    function addEntity() public onlyNewSender {
        EntityOfferRegistry offerRegistry = new EntityOfferRegistry();
        offerRegistry.changeOwner(msg.sender);
        entityRegistries[msg.sender] = offerRegistry;

        emit EntityAdded(msg.sender, offerRegistry);
    }
}
