Truffle project used to deploy smart contracts to different networks.

To deploy:
- $ npm install
- $ npx truffle compile
- $ npx truffle migrate
- $ npx truffle migrate --ropsten
