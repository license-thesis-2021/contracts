var EntityRegistry = artifacts.require("EntityRegistry");

module.exports = function(deployer, network, accounts) {

  EntityRegistry.deployed().then(function(instance) {
    for (i=1; i <= 2; i++) {
      instance.addEntity({from: accounts[i]})
        .then(result => 
          console.log("=>",
                      result.logs[0].args.entityOwner,
                      "owns offer registry",
                      result.logs[0].args.newEntityOfferRegistry));
    }
  });
}